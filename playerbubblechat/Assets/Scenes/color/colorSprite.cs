﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colorSprite : MonoBehaviour
{
    void start()
    {
        GenerateColor();

    }
    public void GenerateColor()
    {
        GetComponent<Renderer>().sharedMaterial.color = Random.ColorHSV();
    }
    public void Reset()
    {
        GetComponent<Renderer>().sharedMaterial.color = Color.white;
    }
}
